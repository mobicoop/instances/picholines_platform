// BASE
import MHeader from '@clientComponents/base/MHeader'
import MFooter from '@clientComponents/base/MFooter'
import Home from '@clientComponents/home/Home'
import DynamicsLines from '@clientComponents/utilities/dynamicsLines/DynamicsLines'
import MDialog from '@clientComponents/utilities/MDialog'

export default {
  MHeader,
  MFooter,
  Home,
  DynamicsLines,
  MDialog,
}