export default {
  // Uncomment to enable dark theme
  //dark: true,
  themes: {
    light: {
      primary: '#95b421',
      accent: '#f29200',
      secondary: '#95b421',
      success: '#95b421',
      info: '#009de0',
      warning: '#FF641E',
      error: '#F03C0E'
    },
    dark: {
      primary: '#80393a',
      accent: '#f29200',
      secondary: '#97b314',
      success: '#00D28C',
      info: '#009de0',
      warning: '#FF641E',
      error: '#F03C0E'
    }
  }
}
